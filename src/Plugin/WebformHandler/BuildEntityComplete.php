<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 03/08/18
 * Time: 08:00
 */

namespace Drupal\webform_entity_builder\Plugin\WebformHandler;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\webform\Annotation\WebformHandler;
use Drupal\webform_entity_builder\Event\EntityBuildEvent;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Form submission handler.
 *
 * THIS HANDLER SHOULD ONLY BE ATTACHED TO WEBFORMS FOR CREATING ENTITIES.
 *
 * @WebformHandler(
 *   id = "webform_entity_builder_build",
 *   label = @Translation("Webform Entity Builder"),
 *   category = @Translation("Webform Entity Builder"),
 *   description = @Translation("Sends an event to build an entity from the webform data provided, and deletes the webform submission afterwards."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 *
 * =======================================================
 * Debugging: Does your webform have an entity type field?
 * A hidden (or value) field called entity_type which gives
 * the type of the entity to be created? And is there an
 * actual plugin to perform the build process?
 * =======================================================
 *
 */
class BuildEntityComplete extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // Only do this if the user has actually finished.
    if ($webform_submission->isCompleted()) {
      $data = $webform_submission->getData();
      // Include the source entity because we want to delete it when finished.
      $data['source_entity'] = $webform_submission;
      // Make sure we have an entity_type field (right sort of form).
      if (!empty($data['entity_type'])) {
        // And fly my pretties...
        EntityBuildEvent::Dispatch($data);
      }
    }
  }
}
