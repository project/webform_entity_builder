<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 20/07/18
 * Time: 14:23
 */

namespace Drupal\webform_entity_builder\Event;

use Drupal\event_scheduler\Event\EventDelayInterface;
use Drupal\housing_event\Event\HousingBaseEvent;

class EntityBuildEvent extends HousingBaseEvent implements EventDelayInterface, EntityBuildEventInterface {

  const NAME = 'webform_entity.build';

  const GROUP = 'webform-entity';

  /**
   * Create and dispatch the relevant activity build event.
   *
   * @param mixed[] $data
   */
  public static function Dispatch($data) {
    $event = new static($data);

    static::doDispatch(static::NAME, $event);
  }

  /**
   * @var mixed[]
   */
  protected $data;

  /**
   * EntityBuildEvent constructor.
   *
   * @param mixed[] $data
   */
  protected function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * @return mixed[]
   */
  public function getData() {
    return $this->data;
  }

  /**
   * @param string $key
   *
   * @return mixed
   */
  public function getKeyedData($key) {
    return $this->data[$key] ?? NULL;
  }

  /**
   * @return mixed
   */
  public function getEntityType() {
    return $this->getKeyedData('entity_type') ?? '';
  }

  /**
   * @return mixed
   */
  public function getEntityTypeId() {
    list($entity_type, ) = explode(':', $this->getEntityType());
    return $entity_type;
  }

  /**
   * @return mixed
   */
  public function getBundle() {
    list(, $bundle, ) = explode(':', $this->getEntityType() . ':');
    return $bundle ?: '*';
  }
}
