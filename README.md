Webform Entity Builder
======================
This module provides support for webform -> entity construction.

WebformHandler
--------------
We provide a webform handler that is attached to the webform and, when the form
has been completed, it compiles the data and launches a delayed event.

The delayed event means that the work on constructing the entity from the
webform data takes place after the current page has been sent to the user. It
provides better UX with less delay.

EntityBuilder
-------------
The event is picked up by this module which finds a matching EntityBuilder
plugin and then runs it on the data from the webform to create the required
entity.
